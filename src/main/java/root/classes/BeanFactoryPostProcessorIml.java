package root.classes;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.*;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import root.classes.donors.NotDefaultBean;
import java.lang.annotation.Annotation;

@Configuration
public class BeanFactoryPostProcessorIml implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        PropertySourcesPlaceholderConfigurer configurer = (PropertySourcesPlaceholderConfigurer) beanFactory.getBean("configurer");
        boolean useMock = (boolean) configurer.getAppliedPropertySources().get("localProperties").getProperty("useMock");
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("defaultBean");
        String beanClassName = beanDefinition.getBeanClassName();

        if (useMock) {
            try {
                Class<?> beanClass = Class.forName(beanClassName);
                Annotation annotation = beanClass.getAnnotation(UseMock.class);
                if (annotation != null) {
                    beanDefinition.setBeanClassName(NotDefaultBean.class.getName());
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer configurer() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(new ClassPathResource("application.yml"));
        configurer.setProperties(yaml.getObject());
        return configurer;
    }

}